package com.sebastian.springboot.app.models.dao;

import java.util.List;

import com.sebastian.springboot.app.models.entity.Cliente;

public interface IClienteDao {

	/***
	 * 
	 * @returns all clients with all data
	 */
	public List<Cliente> findAll();
}
